package com.example;

public class Main {
    public static void main(String[] args) {
        PrimeNumbers primeNumbers=new PrimeNumbers();
        PrimeThread1 primeThread1=new PrimeThread1(primeNumbers);
        PrimeThread2 primeThread2=new PrimeThread2(primeNumbers);
        PrimeThread3 primeThread3=new PrimeThread3(primeNumbers);
        primeThread1.start();
        primeThread1.setPriority(2);
        primeThread2.start();
        primeThread2.setPriority(7);
        primeThread3.start();
        primeThread3.setPriority(4);
    }
}
