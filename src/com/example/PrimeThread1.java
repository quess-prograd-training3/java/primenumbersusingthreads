package com.example;

class PrimeThread1 extends Thread{
    Thread thread;
    PrimeNumbers primeNumbers;

    public PrimeThread1(PrimeNumbers primeNumbers) {
        this.primeNumbers = primeNumbers;
    }
    public void run(){
        primeNumbers.printPrimeNumbers(1,10);
    }
}
class PrimeThread2 extends Thread{
    Thread thread;
    PrimeNumbers primeNumbers;

    public PrimeThread2(PrimeNumbers primeNumbers) {
        this.primeNumbers = primeNumbers;
    }
    public void run(){
        primeNumbers.printPrimeNumbers(20, 30);
    }
}
class PrimeThread3 extends Thread{
    Thread thread;
    PrimeNumbers primeNumbers;

    public PrimeThread3(PrimeNumbers primeNumbers) {
        this.primeNumbers = primeNumbers;
    }
    public void run(){
        primeNumbers.printPrimeNumbers(30,40);
    }
}
