package com.example;

public class PrimeNumbers{
    public synchronized void printPrimeNumbers(int initialNumber, int finalNumber){
        int flag=0;
        for (int iterate = initialNumber; iterate <= finalNumber; iterate++) {
            flag=0;
            for (int iterate2 = 1; iterate2 <= iterate; iterate2++) {
                if(iterate%iterate2==0){
                    flag++;
                }
            }
            if(flag==2){
                System.out.println("Prime Number:- "+iterate);
            }
            else{
                System.out.println("Composite Number:- "+iterate);
            }
        }
    }
}
